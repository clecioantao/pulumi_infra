import * as pulumi from "@pulumi/pulumi";
import * as gitlab from "@pulumi/gitlab";
import {Group, Project} from "@pulumi/gitlab"

const config = new pulumi.Config()

// Visibility level of the group and projects. You will probably want to make it private for your own projects
const visibilityLevel = 'public'

// Create the group that the project(s) will belong under
const group = new Group('Lattice parent group', {name: 'Lattice Group', path: 'lattice-group', visibilityLevel} /*, {import: ''}*/)
export const gitlabGroupId = group.id.apply(id => +id) // Need to convert the groupId to a number https://github.com/pulumi/pulumi-gitlab/issues/28

// Create the projects
const tags =  ['pulumi', 'gitlab', 'gcp', 'google cloud', 'typescript']
const defaultBranch = 'master'
const requestAccessEnabled = false
const sharedRunnersEnabled = true

const infraProject = new Project('Project-infra', {name: 'Lattice Infrastructure', path: 'infra', namespaceId: gitlabGroupId, requestAccessEnabled, visibilityLevel, defaultBranch, tags, sharedRunnersEnabled} /*, {import: ''}*/)
const appProject = new Project('Project-app', {name: 'Lattice App', path: 'app', namespaceId: gitlabGroupId, requestAccessEnabled, visibilityLevel, defaultBranch, tags} /*, {import: ''}*/)


// Protect the infrastructure deploy branches, only allow maintainers to merge and push
new gitlab.BranchProtection('Infra-protect-production-branches', {
    branch: 'production/*',
    mergeAccessLevel: "maintainer",
    project: infraProject.id,
    pushAccessLevel: "maintainer",
})

// Protected variables so GitLab CI builds can use Pulumi

// Option 1, create the variable manually first in GitLab
const infraPulumiTokenVariable = gitlab.ProjectVariable.get("Infra-variable-pulumi-token", pulumi.interpolate `${infraProject.id}:PULUMI_ACCESS_TOKEN`)
const infraGitlabTokenVariable = gitlab.ProjectVariable.get("Infra-variable-gitlab-token", pulumi.interpolate `${infraProject.id}:GITLAB_TOKEN`)

// Option 2, store the the tokens as secret configuration values
// const infraPulumiTokenVariable = new gitlab.ProjectVariable("Infra-variable-pulumi-token", {
//     key: "PULUMI_ACCESS_TOKEN",
//     project: infraProject.id,
//     protected: true,
//     masked: true,
//     value: config.requireSecret<string>('pulumi-token'),
//     environmentScope: '*' // Need to specify this otherwise defaults to 0 which doesn't match all environments
// })
//
// const infraGitlabTokenVariable = new gitlab.ProjectVariable("Infra-variable-gitlab-token", {
//     key: "GITLAB_TOKEN",
//     project: infraProject.id,
//     protected: true,
//     masked: true,
//     value: config.requireSecret<string>('gitlab-token'),
//     environmentScope: '*' // Need to specify this otherwise defaults to 0 which doesn't match all environments
// })

